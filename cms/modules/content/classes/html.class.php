<?php
defined('IN_CMS') or exit('No permission resources.');
pc_base::load_app_func('util','content');
pc_base::load_sys_func('dir');
pc_base::load_app_func('global','content');
pc_base::load_app_func('global','mobile');
class html {
	private $siteid,$url,$html_root,$mobile_root,$queue,$categorys;
	public function __construct() {
		$this->input = pc_base::load_sys_class('input');
		$this->queue = pc_base::load_model('queue_model');
		define('HTML',true);
		self::set_siteid();
		$this->categorys = getcache('category_content_'.$this->siteid,'commons');
		$this->url = pc_base::load_app_class('url', 'content');
		$this->html_root = SYS_HTML_ROOT;
		$this->mobile_root = SYS_MOBILE_ROOT;
		$this->sitelist = getcache('sitelist','commons');
	}

	/**
	 * 生成内容页
	 * @param  $file 文件地址
	 * @param  $data 数据
	 * @param  $array_merge 是否合并
	 * @param  $action 方法
	 * @param  $upgrade 是否是升级数据
	 */
	public function show($file, $data = '', $array_merge = 1,$action = 'add',$upgrade = 0) {
		if (strpos($data['url'], 'index.php?')!==false) return false;
		if($upgrade) $file = '/'.ltrim($file,WEB_PATH);
		$allow_visitor = 1;
		$id = $data['id'];
		$remains = true;
		if($array_merge) {
			$data = new_stripslashes($data);
			$data = array_merge($data['system'],$data['model']);
		}
		//通过rs获取原始值
		$rs = $data;
		if(isset($data['paginationtype'])) {
			$paginationtype = $data['paginationtype'];
			$maxcharperpage = $data['maxcharperpage'];
		} else {
			$paginationtype = 0;
		}
		$catid = $data['catid'];
		$CATEGORYS = $this->categorys;
		$CAT = $CATEGORYS[$catid];
		$CAT['setting'] = string2array($CAT['setting']);
		define('STYLE',$CAT['setting']['template_list']);
		define('SITEID', $this->siteid);
		define('ISHTML', $CAT['setting']['content_ishtml']);
		define('IS_HTML', $CAT['setting']['content_ishtml']);
		if(!$CAT['setting']['content_ishtml']) return false;

		//最顶级栏目ID
		$arrparentid = explode(',', $CAT['arrparentid']);
		$top_parentid = $arrparentid[1] ? $arrparentid[1] : $catid;
		
		//$file = '/'.$file;
		//添加到发布点队列
		//当站点为非系统站点
		
		if($this->siteid!=1) {
			$site_dir = $this->sitelist[$this->siteid]['dirname'];
			$file = $this->html_root.'/'.$site_dir.$file;
		}
		
		$this->queue->add_queue($action,$file,$this->siteid);
		
		$modelid = $CAT['modelid'];
		require_once CACHE_MODEL_PATH.'content_output.class.php';
		$content_output = new content_output($modelid,$catid,$CATEGORYS);
		$output_data = $content_output->get($data);
		extract($output_data);
		if(module_exists('comment')) {
			$allow_comment = isset($allow_comment) ? $allow_comment : 1;
		} else {
			$allow_comment = 0;
		}
		$this->db = pc_base::load_model('content_model');
		$this->db->set_model($modelid);
		//上一页
		$previous_page = $this->db->get_one("`catid` = '$catid' AND `id`<'$id' AND `status`=99",'*','id DESC');
		//下一页
		$next_page = $this->db->get_one("`catid`= '$catid' AND `id`>'$id' AND `status`=99",'*','id ASC');
		
		if(empty($previous_page)) {
			$previous_page = array('title'=>L('first_page','','content'), 'thumb'=>IMG_PATH.'nopic_small.gif', 'url'=>'javascript:alert(\''.L('first_page','','content').'\');');
		}
		if(empty($next_page)) {
			$next_page = array('title'=>L('last_page','','content'), 'thumb'=>IMG_PATH.'nopic_small.gif', 'url'=>'javascript:alert(\''.L('last_page','','content').'\');');
		}
	
		$title = clearhtml($title);
		//SEO
		$seo_keywords = '';
		if(!empty($keywords)) $seo_keywords = implode(',',$keywords);
		$siteid = $this->siteid;
		$SEO = seo($siteid, $catid, $title, $description, $seo_keywords);
		
		$ishtml = 1;
		$template = $template ? $template : $CAT['setting']['show_template'];
		
		//分页处理
		$pages = '';
		$titles = array();
		if($paginationtype==1) {
			//自动分页
			if($maxcharperpage < 10) $maxcharperpage = 500;
			$contentpage = pc_base::load_app_class('contentpage');
			$content = $contentpage->get_data($content,$maxcharperpage);
		}
	
		if($paginationtype!=0) {
			//手动分页
			$CONTENT_POS = strpos($content, '[page]');
			if($CONTENT_POS !== false) {
				$this->url = pc_base::load_app_class('url', 'content');	
				$contents = array_filter(explode('[page]', $content));
				$pagenumber = dr_count($contents);
				if (strpos($content, '[/page]')!==false && ($CONTENT_POS<7)) {
					$pagenumber--;
				}
				for($i=1; $i<=$pagenumber; $i++) {
					$upgrade = $upgrade ? '/'.ltrim($file,WEB_PATH) : '';
					list($pageurls[$i], $showurls[$i]) = $this->url->show($id, $i, $catid, $data['inputtime'],'','','edit',$upgrade);
				}
				$END_POS = strpos($content, '[/page]');
				if($END_POS !== false) {
					if($CONTENT_POS>7) {
						$content = '[page]'.$title.'[/page]'.$content;
					}
					if(preg_match_all("|\[page\](.*)\[/page\]|U", $content, $m, PREG_PATTERN_ORDER)) {
						foreach($m[1] as $k=>$v) {
							$p = $k+1;
							$titles[$p]['title'] = clearhtml($v);
							$titles[$p]['url'] = $pageurls[$p][0];
						}
					}
				}
				//生成分页
				foreach ($pageurls as $page=>$urls) {
					$pages = content_pages($pagenumber,$page,$showurls);
					//判断[page]出现的位置是否在第一位 
					if($CONTENT_POS<7) {
						$content = $contents[$page];
					} else {
						if ($page==1 && !empty($titles)) {
							$content = $title.'[/page]'.$contents[$page-1];
						} else {
							$content = $contents[$page-1];
						}
					}
					if($titles) {
						list($title, $content) = explode('[/page]', $content);
						$content = trim($content);
						if(strpos($content,'</p>')===0) {
							$content = '<p>'.$content;
						}
						if(stripos($content,'<p>')===0) {
							$content = $content.'</p>';
						}
					}
					$pagefile = $urls[1];
					if($this->siteid!=1) {
						$pagefile = $this->html_root.'/'.$site_dir.$pagefile;
					}
					$this->queue->add_queue($action,$pagefile,$this->siteid);
					$pagefile = CMS_PATH.$pagefile;
					ob_start();
					include template('content', $template);
					$this->createhtml($pagefile);
					if($this->sitelist[$this->siteid]['mobilemode']==1 && $this->sitelist[$this->siteid]['mobilehtml']==1) {
						$mobilepagefile = CMS_PATH.$this->mobile_root.'/'.str_replace(CMS_PATH,'',$pagefile);
						ob_start();
						$url = str_replace(array($this->sitelist[$this->siteid]['domain'], 'm=content'), array($this->sitelist[$this->siteid]['mobile_domain'], 'm=mobile'), $url);
						include template('mobile', $template);
						$this->createhtml($mobilepagefile);
					}
				}
				//生成手机分页
				if (!$this->sitelist[$this->siteid]['mobilemode']) {
					for($i=1; $i<=$pagenumber; $i++) {
						$upgrade = $upgrade ? '/'.ltrim($file,WEB_PATH) : '';
						list($pageurls[$i], $showurls[$i]) = $this->url->show($id, $i, $catid, $data['inputtime'],'','','edit',$upgrade,1);
					}
					$titles = array();
					$END_POS = strpos($content, '[/page]');
					if($END_POS !== false) {
						if($CONTENT_POS>7) {
							$content = '[page]'.$title.'[/page]'.$content;
						}
						if(preg_match_all("|\[page\](.*)\[/page\]|U", $content, $m, PREG_PATTERN_ORDER)) {
							foreach($m[1] as $k=>$v) {
								$p = $k+1;
								$titles[$p]['title'] = clearhtml($v);
								$titles[$p]['url'] = $pageurls[$p][0];
							}
						}
					}
					foreach ($pageurls as $page=>$urls) {
						$pages = content_pages($pagenumber,$page,$showurls);
						//判断[page]出现的位置是否在第一位 
						if($CONTENT_POS<7) {
							$content = $contents[$page];
						} else {
							if ($page==1 && !empty($titles)) {
								$content = $title.'[/page]'.$contents[$page-1];
							} else {
								$content = $contents[$page-1];
							}
						}
						if($titles) {
							list($title, $content) = explode('[/page]', $content);
							$content = trim($content);
							if(strpos($content,'</p>')===0) {
								$content = '<p>'.$content;
							}
							if(stripos($content,'<p>')===0) {
								$content = $content.'</p>';
							}
						}
						$pagefile = $urls[1];
						if($this->siteid!=1) {
							$pagefile = $this->html_root.'/'.$site_dir.$pagefile;
						}
						$pagefile = CMS_PATH.$pagefile;
						if($this->sitelist[$this->siteid]['mobilehtml']==1) {
							ob_start();
							$url = str_replace(array($this->sitelist[$this->siteid]['domain'], 'm=content'), array($this->sitelist[$this->siteid]['mobile_domain'], 'm=mobile'), $url);
							include template('mobile', $template);
							$this->createhtml($pagefile);
						}
					}
				}
				return true;
			}
		}
		//分页处理结束
		$file = CMS_PATH.$file;
		ob_start();
		include template('content', $template);
		$this->createhtml($file);
		if($this->sitelist[$this->siteid]['mobilehtml']==1) {
			$mobilefile = CMS_PATH.$this->mobile_root.'/'.str_replace(CMS_PATH,'',$file);
			ob_start();
			$url = str_replace(array($this->sitelist[$this->siteid]['domain'], 'm=content'), array($this->sitelist[$this->siteid]['mobile_domain'], 'm=mobile'), $url);
			include template('mobile', $template);
			$this->createhtml($mobilefile);
		}
		return true;
	}

	/**
	 * 生成栏目列表
	 * @param $catid 栏目id
	 * @param $page 当前页数
	 */
	public function category($catid, $page = 0, $maxsize = 0) {
		$CAT = $this->categorys[$catid];
		if (strpos($CAT['url'], 'index.php?')!==false) return false;
		if (is_array($CAT)) {
			@extract($CAT);
		}
		if(!$ishtml){
			return false;
		}
		if(!$catid){
			return false;
		}
		$CATEGORYS = $this->categorys;
		if(!isset($CATEGORYS[$catid])){
			return false;
		}
		$siteid = $CAT['siteid'];
		$copyjs = '';
		$setting = string2array($setting);
		if(!$setting['meta_title']) $setting['meta_title'] = $catname;
		$SEO = seo($siteid, '',$setting['meta_title'],$setting['meta_description'],$setting['meta_keywords']);
		define('STYLE',$setting['template_list']);
		define('SITEID', $siteid);
		define('ISHTML', $setting['ishtml']);
		define('IS_HTML', $setting['ishtml']);

		$page = intval($page);
		$parentdir = $CAT['parentdir'];
		$catdir = $CAT['catdir'];
		//检查是否生成到根目录
		$create_to_html_root = $CAT['sethtml'];
		//$base_file = $parentdir.$catdir.'/';
		//生成地址
		if($CAT['create_to_html_root']) $parentdir = '';
		
		//获取父级的配置，看是否生成静态，如果是动态则直接把父级目录调过来为生成静态目录所用
		$parent_setting = string2array($CATEGORYS[$CAT['parentid']]['setting']);
		if($parent_setting['ishtml']==0 && $setting['ishtml']==1){
			$parentdir = $CATEGORYS[$CAT['parentid']]['catdir'].'/';
		}
		
		$base_file = $this->url->get_list_url($setting['category_ruleid'],$parentdir, $catdir, $catid, $page);
		$base_file = '/'.$base_file;
		
		//非系统站点时，生成到指定目录
		if($this->siteid!=1) {
			$site_dir = $this->sitelist[$this->siteid]['dirname'];
			if($create_to_html_root) {
				$base_file = '/'.$site_dir.$base_file;
			} else {
				$base_file = '/'.$site_dir.$this->html_root.$base_file;
			}
		} 
		//判断二级域名是否直接绑定到该栏目
		$root_domain = preg_match('/^((http|https):\/\/)([a-z0-9\-\.]+)\/$/',$CAT['url']) ? 1 : 0;
		$count_number = substr_count($CAT['url'], '/');
		$urlrules = getcache('urlrules','commons');
		$urlrules = explode('|',$urlrules[$category_ruleid]);
		
		if($create_to_html_root) {
			if($this->siteid==1) {
				$file = CMS_PATH.$base_file;
				$mobilefile = CMS_PATH.$this->mobile_root.'/'.$base_file;
			} else {
				$file = CMS_PATH.substr($this->html_root,1).$base_file;
				$mobilefile = CMS_PATH.$this->mobile_root.'/'.substr($this->html_root,1).$base_file;
			}
			//添加到发布点队列
			$this->queue->add_queue('add',$base_file,$this->siteid);
			//评论跨站调用所需的JS文件
			if(substr($base_file, -10)=='index.html' && $count_number==3) {
				$copyjs = 1;
				$this->queue->add_queue('add',$base_file,$this->siteid);
			}
			//URLRULES
			if($CAT['isdomain']) {
				$second_domain = 1;
				foreach ($urlrules as $_k=>$_v) {
					$urlrules[$_k] = $_v;
				}
			} else {
				foreach ($urlrules as $_k=>$_v) {
					$urlrules[$_k] = '/'.$_v;
				}
			}
		} else {
			$file = CMS_PATH.substr($this->html_root,1).$base_file;
			$mobilefile = CMS_PATH.$this->mobile_root.'/'.substr($this->html_root,1).$base_file;
			//添加到发布点队列
			$this->queue->add_queue('add',$this->html_root.$base_file,$this->siteid);
			//评论跨站调用所需的JS文件
			if(substr($base_file, -10)=='index.html' && $count_number==3) {
				$copyjs = 1;
				$this->queue->add_queue('add',$this->html_root.$base_file,$this->siteid);
			}		
			//URLRULES
			$htm_prefix = $root_domain ? '' : $this->html_root;
			$htm_prefix = rtrim(WEB_PATH,'/').$htm_prefix;
			if($CAT['isdomain']) {
				$second_domain = 1;
			} else {
				$second_domain = 0;//判断该栏目是否绑定了二级域名或者上级栏目绑定了二级域名，存在的话，重新构造列表页url规则
				foreach ($urlrules as $_k=>$_v) {
					$urlrules[$_k] = $htm_prefix.'/'.$_v;
				}
			}
		}
		
		$arrchild_arr = $CATEGORYS[$parentid]['arrchildid'];
		if($arrchild_arr=='') $arrchild_arr = $CATEGORYS[$catid]['arrchildid'];
		$arrchild_arr = explode(',',$arrchild_arr);
		array_shift($arrchild_arr);
		$this->category_db = pc_base::load_model('category_model');
		$arrchild_arr = $this->category_db->select(array('catid'=>$arrchild_arr), '*', '', 'listorder ASC, catid ASC');
		foreach ($arrchild_arr as $cache) {
			$arrchild[] = $cache['catid'];
		}
		$arrchild_arr = $arrchild;
		foreach ($arrchild_arr as $mcatid => $cache) {
			$arrchild_setting = string2array($CATEGORYS[$cache]['setting']);
			if (!$arrchild_setting['isleft'] || $arrchild_setting['disabled']) {
				unset($arrchild_arr[$mcatid]);
				continue;
			}
			$arrchild_arr[$mcatid] = $cache;
		}

		if($type==0) {
			$template = $setting['category_template'] ? $setting['category_template'] : 'category';
			$template_list = $setting['list_template'] ? $setting['list_template'] : 'list';
			$template = $child ? $template : $template_list;
			$arrparentid = explode(',', $arrparentid);
			$top_parentid = $arrparentid[1] ? $arrparentid[1] : $catid;
			$array_child = array();
			$self_array = explode(',', $arrchildid);
			foreach ($self_array as $arr) {
				if($arr!=$catid) $array_child[] = $arr;
			}
			$arrchildid = implode(',', $array_child);
			//URL规则
			$urlrules = implode('~', $urlrules);
			$urlrules_arr = explode('~',$urlrules);
			$year = date('Y',$time);
			$month = date('m',$time);
			$day = date('d',$time);
			$category_dir = $this->get_categorydir($catid);
			$categoryurl = str_replace(array('{$categorydir}','{$catdir}','{$year}','{$month}','{$day}','{$catid}','{$page}'),array($category_dir,$CAT['catdir'],$year,$month,$day,$catid,'{page}'),$urlrules_arr);
			
			//绑定域名时，设置$catdir 为空
			if($root_domain) $parentdir = $catdir = '';
			if($second_domain) {
				$parentdir = '';
				$parentdir = str_replace($catdir.'/', '', $CAT['url']);
			}
			
			$GLOBALS['URL_ARRAY'] = array('categorydir'=>$parentdir, 'catdir'=>$catdir, 'catid'=>$catid);
			
			$this->db = pc_base::load_model('content_model');
			$this->db->set_model($modelid);
			if ($child) {
				$pagenumber = $this->db->count(array('catid'=>explode(',', $arrchildid), 'status'=>99));
			} else {
				$pagenumber = $this->db->count(array('catid'=>$catid, 'status'=>99));
			}
			$url_arr[0] = $categoryurl[0];
			$url_arr[1] = $categoryurl[1];
			$pagesize = (int)$setting['pagesize'];
			!$pagesize && $pagesize = 10;
			isset($maxsize) && $maxsize && (int)$maxsize > 0 && $pagenumber = $pagesize * $maxsize;
			if($pagenumber > $pagesize) {
				$pages = category_pages($pagenumber,$page,$pagesize,$url_arr);
			}
		} else {
		//单网页
			$datas = $this->page($catid);
			if($datas) extract($datas);
			$template = $setting['page_template'] ? $setting['page_template'] : 'page';
			$keywords = $keywords ? $keywords : $setting['meta_keywords'];
			$SEO = seo($siteid, 0, $setting['meta_title'] ? $setting['meta_title'] : $title,$setting['meta_description'],$keywords);
		}
		ob_start();
		include template('content',$template);
		$this->createhtml($file, $copyjs);
		if($this->sitelist[$this->siteid]['mobilehtml']==1) {
			if($type==0) {
				$mobile_category = !$this->sitelist[$this->siteid]['mobilemode'] ? $this->mobile_root : '';
				$url_arr[0] = $mobile_category.$categoryurl[0];
				$url_arr[1] = $mobile_category.$categoryurl[1];
				if($pagenumber > $pagesize) {
					$pages = category_pages($pagenumber,$page,$pagesize,$url_arr);
				}
			}
			ob_start();
			$url = str_replace(array($this->sitelist[$this->siteid]['domain'], 'm=content'), array($this->sitelist[$this->siteid]['mobile_domain'], 'm=mobile'), $url);
			include template('mobile',$template);
			$this->createhtml($mobilefile);
		}
		return true;
	}
	/**
	 * 更新首页
	 */
	public function index() {
		if($this->siteid==1) {
			$file = CMS_PATH.'index.html';
			$mobilefile = CMS_PATH.$this->mobile_root.'/index.html';
			$mapfile = CMS_PATH.$this->mobile_root.'/map.html';
			//添加到发布点队列
			$this->queue->add_queue('edit','/index.html',$this->siteid);
		} else {
			$site_dir = $this->sitelist[$this->siteid]['dirname'];
			$file = $this->html_root.'/'.$site_dir.'/index.html';
			$mobilefile = $this->mobile_root.'/'.$this->html_root.'/'.$site_dir.'/index.html';
			$mapfile = $this->mobile_root.'/'.$this->html_root.'/'.$site_dir.'/map.html';
			//添加到发布点队列
			$this->queue->add_queue('edit',$file,$this->siteid);
			$file = CMS_PATH.$file;
			$mobilefile = CMS_PATH.$mobilefile;
			$mapfile = CMS_PATH.$mapfile;
		}
		define('SITEID', $this->siteid);
		//SEO
		$SEO = seo($this->siteid);
		$siteid = $this->siteid;
		$CATEGORYS = $this->categorys;
		$style = $this->sitelist[$siteid]['default_style'];
		define('IS_HTML', $this->sitelist[$siteid]['ishtml']);
		if($this->sitelist[$siteid]['ishtml']==1) {
			ob_start();
			include template('content','index',$style);
			$pc = $this->createhtml($file, 1);
			if($this->sitelist[$siteid]['mobilehtml']==1) {
				ob_start();
				include template('mobile','maps',$style);
				$this->createhtml($mapfile);
				ob_start();
				include template('mobile','index',$style);
				$mobile = $this->createhtml($mobilefile);
			}
		}
		return L('电脑端 （'.format_file_size($pc).'），移动端 （'.format_file_size($mobile).'）');
	}
	/**
	 * 单网页
	 * @param $catid
	 */
	public function page($catid) {
		$this->page_db = pc_base::load_model('page_model');
		$data = $this->page_db->get_one(array('catid'=>$catid));
		return $data;
	}
	/**
	* 写入文件
	* @param $file 文件路径
	* @param $copyjs 是否复制js，跨站调用评论时，需要该js
	*/
	private function createhtml($file, $copyjs = '') {
		$data = ob_get_contents();
		ob_clean();
		$dir = dirname($file);
		if(!is_dir($dir)) {
			mkdir($dir, 0777,1);
		}
		if ($copyjs && !file_exists($dir.'/js.html')) {
			@copy(PC_PATH.'modules/content/templates/js.html', $dir.'/js.html');
		}
		$strlen = file_put_contents($file, $data, LOCK_EX);
		@chmod($file,0777);
		if(!is_writable($file)) {
			$file = str_replace(CMS_PATH,'',$file);
			show_error(L('file').'：'.$file.'<br>'.L('not_writable'));
		}
		return $strlen;
	}

	/**
	 * 获取父栏目路径
	 * @param $catid
	 * @param $dir
	 */
	private function get_categorydir($catid, $dir = '') {
		$setting = array();
		$setting = string2array($this->categorys[$catid]['setting']);
		if ($setting['create_to_html_root']) return $dir;
		if ($this->categorys[$catid]['parentid']) {
			$dir = $this->categorys[$this->categorys[$catid]['parentid']]['catdir'].'/'.$dir;
			return $this->get_categorydir($this->categorys[$catid]['parentid'], $dir);
		} else {
			return $dir;
		}
	}
	/**
	 * 设置当前站点id
	 */
	private function set_siteid() {
		if(defined('IS_ADMIN') && IS_ADMIN) {
			$this->siteid = $GLOBALS['siteid'] = get_siteid();
		} else {
			if (param::get_cookie('siteid')) {
				$this->siteid = $GLOBALS['siteid'] = param::get_cookie('siteid');
			} else {
				$this->siteid = $GLOBALS['siteid'] = 1;
			}
		}
	}
	/**
	* 生成相关栏目列表、只生成前5页
	* @param $catid
	*/
	public function create_relation_html($catid) {
		$this->db = pc_base::load_model('content_model');
		$CAT = $this->categorys[$catid];
		$array_child = array();
		$self_array = explode(',', $CAT['arrchildid']);
		foreach ($self_array as $arr) {
			if($arr!=$catid) $array_child[] = $arr;
		}
		$arrchildid = implode(',', $array_child);
		$this->db->set_model($CAT['modelid']);
		$setting = string2array($CAT['setting']);
		$pagesize = (int)$setting['pagesize'];
		$maxsize = (int)$setting['maxsize'];
		$maxsize && $maxsize = $maxsize;
		!$pagesize && $pagesize = 10;
		if ($arrchildid) {
			$pagenumber = $this->db->count(array('catid'=>explode(',', $arrchildid)));
		} else {
			$pagenumber = $this->db->count(array('catid'=>$catid));
		}
		!$maxsize && $maxsize = ceil($pagenumber/$pagesize);
		$maxsize > 10 && $maxsize = 10;
		!$setting['maxsize'] && $maxsize > 5 && $maxsize = 5;
		!$maxsize && $maxsize = 2;
		for($page = 1; $page < $maxsize + 1; $page++) {
			$this->category($catid,$page,$setting['maxsize'] ? $maxsize : 0);
		}
		//检查当前栏目的父栏目，如果存在则生成
		$arrparentid = $this->categorys[$catid]['arrparentid'];
		if($arrparentid) {
			$arrparentid = explode(',', $arrparentid);
			foreach ($arrparentid as $catid) {
				if($catid) $this->category($catid,1,$setting['maxsize'] ? $maxsize : 0);
			}
		}
	}
}