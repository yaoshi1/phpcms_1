
	<div class="form-group">
      <label class="col-md-2 control-label">文本框长度</label>
      <div class="col-md-9">
            <label><input type="text" name="setting[size]" value="50" size="10" class="form-control"></label>
      </div>
    </div>
	<div class="form-group">
      <label class="col-md-2 control-label">默认值</label>
      <div class="col-md-9">
            <label><input type="text" name="setting[defaultvalue]" value="" size="40" class="form-control"></label>
      </div>
    </div>
	<div class="form-group">
      <label class="col-md-2 control-label">是否为密码框</label>
      <div class="col-md-9">
            <div class="mt-radio-inline">
          <label class="mt-radio mt-radio-outline"><input type="radio" name="setting[ispassword]" value="1"> 是 <span></span></label>
          <label class="mt-radio mt-radio-outline"><input type="radio" name="setting[ispassword]" value="0" checked> 否 <span></span></label>
        </div></label>
      </div>
    </div>
