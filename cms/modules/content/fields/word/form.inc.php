	function word($field, $value, $fieldinfo) {
		extract($fieldinfo);
		$setting = string2array($setting);
		$str = load_js(JS_PATH.'layui/layui.js');
		$str .= '<script>';
		$str .= 'layui.use(\'upload\', function () {';
		$str .= '	var upload = layui.upload;';
		$str .= '	upload.render({';
		$str .= '		elem:\'#'.$field.'\',';
		$str .= '		accept:\'file\',';
		$str .= '		field:\'file_upload\',';
		$str .= '		url: \''.WEB_PATH.'api.php?op=get_word&module=content&catid='.intval($this->input->get('catid')).'&userid='.$this->userid.'&siteid='.$this->siteid.'&watermark='.$watermark.'&attachment='.$attachment.'&image_reduce='.$image_reduce.'\',';
		$str .= '		exts: \'docx\',';
		$str .= '		done: function(data){';
		$str .= '			if(data.code == 1){';
		$str .= '				dr_tips(data.code, data.msg);';
		$str .= '				var arr = data.data;';
		$str .= '				$(\'#'.$field.'_word\').val(arr.file);';
		$str .= '				$(\'#title\').val(arr.title);';
		$str .= '				if ($(\'#keywords\').length > 0) {';
		$str .= '					$(\'#keywords\').val(arr.keyword);';
		$str .= '					$(\'#keywords\').tagsinput(\'add\', arr.keyword);';
		$str .= '				}';
		if (SYS_EDITOR) {
			$str .= '				CKEDITOR.instances[\'content\'].setData(arr.content);';
		} else {
			$str .= '				UE.getEditor(\'content\').setContent(arr.content);';
		}
		$str .= '			}else{';
		$str .= '				dr_tips(data.code, data.msg);';
		$str .= '			}';
		$str .= '		}';
		$str .= '	});';
		$str .= '});';
		$str .= '</script>';
		if(!$value) $value = '';
		$errortips = $this->fields[$field]['errortips'];
		if($errortips || $minlength) $this->formValidator .= '$("#'.$field.'_word").formValidator({onshow:"",onfocus:"'.$errortips.'"}).inputValidator({min:1,onerror:"'.$errortips.'"});';
		//if (defined('IS_ADMIN') && IS_ADMIN) {
			return '<input type="hidden" name="info['.$field.']" id="'.$field.'_word" value="'.$value.'" '.$formattribute.' '.$css.'><button type="button" class="layui-btn layui-btn-sm" id="'.$field.'"><i class="layui-icon">&#xe67c;</i>'.L('import_word').'</button>'.$str;
		//} else {
			//return L('import_wxurl_publish');
		//}
	}
