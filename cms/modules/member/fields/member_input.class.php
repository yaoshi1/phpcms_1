<?php
class member_input {
	var $modelid;
	var $fields;
	var $data;

	function __construct($modelid) {
		$this->input = pc_base::load_sys_class('input');
		$this->db = pc_base::load_model('sitemodel_field_model');
		$this->db_pre = $this->db->db_tablepre;
		$this->modelid = $modelid;
		$this->fields = getcache('model_field_'.$modelid,'model');

		//初始化附件类
		pc_base::load_sys_class('download','',0);
		$this->siteid = param::get_cookie('siteid');
		$this->download = new download('content','0',$this->siteid);

	}

	function get($data) {
		$_roleid = param::get_cookie('_roleid') ? param::get_cookie('_roleid') : $_SESSION['roleid'];
		$_groupid = param::get_cookie('_groupid');
		$this->data = $data;
		$model_cache = getcache('member_model', 'commons');
		$this->db->table_name = $this->db_pre.$model_cache[$this->modelid]['tablename'];

		$info = array();
		$debar_filed = array('catid','title','style','thumb','status','islink','description');
		if(is_array($data)) {
			foreach($data as $field=>$value) {
				if(defined('IS_ADMIN') && IS_ADMIN) {
					if($this->fields[$field]['disabled'] || $this->fields[$field]['iscore'] || check_in($_roleid, $this->fields[$field]['unsetroleids'])) continue;
				} else {
					if($this->fields[$field]['disabled'] || $this->fields[$field]['iscore'] || !$this->fields[$field]['isadd'] || check_in($_groupid, $this->fields[$field]['unsetgroupids'])) continue;
				}
				if($data['islink']==1 && !in_array($field,$debar_filed)) continue;
				$field = safe_replace($field);
				$name = $this->fields[$field]['name'];
				$minlength = $this->fields[$field]['minlength'];
				$maxlength = $this->fields[$field]['maxlength'];
				$pattern = $this->fields[$field]['pattern'];
				$errortips = $this->fields[$field]['errortips'];
				if(empty($errortips)) $errortips = "$name 不符合要求！";
				$length = empty($value) ? 0 : (is_string($value) ? mb_strlen($value) : dr_strlen($value));
				if($minlength && $length < $minlength && !$isimport) {
					if (IS_ADMIN) {
						dr_admin_msg(0, "$name 不得少于 $minlength 个字符！", array('field' => $field));
					} else {
						dr_msg(0, "$name 不得少于 $minlength 个字符！", array('field' => $field));
					}
				}
				if (!array_key_exists($field, $this->fields)) {
					if (IS_ADMIN) {
						dr_admin_msg(0, '模型中不存在'.$field.'字段', array('field' => $field));
					} else {
						dr_msg(0, '模型中不存在'.$field.'字段', array('field' => $field));
					}
				}
				if($maxlength && $length > $maxlength && !$isimport) {
					if (IS_ADMIN) {
						dr_admin_msg(0, "$name 不得超过 $maxlength 个字符！", array('field' => $field));
					} else {
						dr_msg(0, "$name 不得超过 $maxlength 个字符！", array('field' => $field));
					}
				} else {
					str_cut($value, $maxlength);
				}
				if($pattern && $length && !preg_match($pattern, $value) && !$isimport) {
					if (IS_ADMIN) {
						dr_admin_msg(0, $errortips, array('field' => $field));
					} else {
						dr_msg(0, $errortips, array('field' => $field));
					}
				}
				if($this->fields[$field]['isunique'] && $this->db->get_one(array($field=>$value),$field) && ROUTE_A != 'edit') {
					if (IS_ADMIN) {
						dr_admin_msg(0, "$name 的值不得重复！", array('field' => $field));
					} else {
						dr_msg(0, "$name 的值不得重复！", array('field' => $field));
					}
				}
				$func = $this->fields[$field]['formtype'];
				if(method_exists($this, $func)) $value = $this->$func($field, $value);

				$info[$field] = $value;
			}
		}
		return $info;
	}
}?>