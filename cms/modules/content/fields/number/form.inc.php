	function number($field, $value, $fieldinfo) {
		extract($fieldinfo);
		$setting = string2array($setting);
		// 表单宽度设置
		$width = is_mobile(0) ? '100%' : 400;
		if(!$value) $value = $defaultvalue;
		return "<label><input type='text' name='info[$field]' id='$field' value='$value' class='form-control {$css}' style='width:".$width.(is_numeric($width) ? "px" : "").";' {$formattribute}></label>";
	}
