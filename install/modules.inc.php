<?php 
$CMS_VERSION_NAME = 'CMS V10';

$CMS_MODULES = array(
'name'=>array('announce','comment','link','vote','message','mood','poster','formguide','tag','bdts','custom','customfield','fclient','guestbook','import','slider','sqltoolplus','taglist'),
'modulename'=>array('网站公告','评论','友情链接','投票','短消息','心情指数','广告管理','表单向导','标签向导','百度推送','资料','变量','客户站群','留言板','外部导入','幻灯片','SQL工具','TAG管理'),
'modules'=>array('announce'=>'网站公告','comment'=>'评论','link'=>'友情链接','vote'=>'投票','message'=>'短消息','mood'=>'心情指数','poster'=>'广告管理','formguide'=>'表单向导','tag'=>'标签向导','bdts'=>'百度推送','custom'=>'资料','customfield'=>'变量','fclient'=>'客户站群','guestbook'=>'留言板','import'=>'外部导入','slider'=>'幻灯片','sqltoolplus'=>'SQL工具','taglist'=>'TAG管理'),
);
?>